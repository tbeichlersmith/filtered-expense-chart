# Filtered Expense Chart
Only look at expenses charged to certain accounts.

### Resources
- [Creating or Modifying Reports or Charts](https://cvs.gnucash.org/docs/C/gnucash-help/report-create.html)
- [Custom Reports](https://wiki.gnucash.org/wiki/Custom_Reports)
- [HTML Chart Tutorial](https://wiki.gnucash.org/wiki/Tutorial_for_html-chart.scm)

Able to view re-named sample report after linking config file and restarting GnuCash.
```
cd ~/.config/gnucash/
ln -s /path/to/this/repo/config-user.scm .
# restart gnucash
```

### Road Map
Basic idea is to combine the "Expense over Time" categorical-bar-chart
with the cashflow chart. This way I can break up the total income and
total expense bars in the cashflow chart by the origin/destination.
